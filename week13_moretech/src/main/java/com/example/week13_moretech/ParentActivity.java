package com.example.week13_moretech;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ParentActivity extends AppCompatActivity {
    Button seekChildBnt;
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
        setTitle("上一级页面");
        seekChildBnt= findViewById(R.id.seekChildBnt);
        seekChildBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ParentActivity.this,ChildActivity.class);
                startActivity(intent);
            }
        });
    }
}