package com.example.week13_moretech;

import android.app.Application;

public class MyApplication extends Application {
   private String userName;
   private String orgName;
   @Override public void onCreate() {
      super.onCreate();
      setUserName("rth");
      setOrgName("nb");
   }

   public String getUserName() {
      return userName;
   }

   public void setUserName(String userName) {
      this.userName = userName;
   }

   public String getOrgName() {
      return orgName;
   }

   public void setOrgName(String orgName) {
      this.orgName = orgName;
   }
}
