package com.example.week13_moretech;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.os.Bundle;

public class ChildActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);
        if (NavUtils.getParentActivityName(ChildActivity.this) != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}