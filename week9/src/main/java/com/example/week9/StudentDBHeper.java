package com.example.week9;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by HP on 2022/4/28.
 */

public class StudentDBHeper extends SQLiteOpenHelper {

    private static final String DB_NAME = "students.db";
    private static final String TBL_NAME = "student";
    private SQLiteDatabase stuDB;

    public StudentDBHeper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        this.stuDB = database;
        String CREATE_TBL = "CREATE TABLE student(_id integer primar key ,num text,name text)";
        stuDB.execSQL(CREATE_TBL);
    }

    public void insert(ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TBL_NAME, null, values);
        db.close();
    }

    public Cursor query() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TBL_NAME,
                null,
                null,
                null,
                null,
                null,
                null);
        return cursor;
    }

    public void del(int id) {
        if (stuDB == null)
            stuDB = getWritableDatabase();
        stuDB.delete(TBL_NAME, "_id=?", new String[]{String.valueOf(id)});
    }

    public void close() {
        if (stuDB != null)
            stuDB.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
