package com.mingrisoft.week3_4;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{


    private Button clickBtn;
    private EditText et;
    private EditText et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et = (EditText)findViewById(R.id.textpassword2);
        et2 = (EditText)findViewById(R.id.textid2);

        clickBtn=(Button) findViewById(R.id.btn);
        clickBtn.setOnClickListener(new View.OnClickListener()
        {


            @Override
            public void onClick(View v)
            {

                String id="201958501325";
                String password="222222";
                if (id.equals(et2.getText().toString())==false)
                {
                    Toast.makeText(MainActivity.this, "学号错误",Toast.LENGTH_SHORT).show();
                }else{
                    if (password.equals(et.getText().toString())==false)
                    {
                        Toast.makeText(MainActivity.this,"密码错误",Toast.LENGTH_SHORT).show();

                    }else{
                        //新① 创建Bundle对象准备数据
                        Bundle bundle = new Bundle();
                        bundle.putString("msg", et2.getText().toString());

                        //新② 创建Intent对象，向Intent对象添加数据
                        Intent intent =new Intent(MainActivity.this, SecondActivity.class);
                        intent.putExtras(bundle);

                        //新③ 用startActivity()方法启动新的Activity
                        startActivity(intent);


                    }
                }

            }
        });
    };
}