package com.mingrisoft.week3_4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    //定义组件对象
    private TextView msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //创建组件对应的对象
        msg = (TextView) findViewById(R.id.welcome);

        //新④ 用Intent对象接受并处理数据
        Intent intent = getIntent();

        msg.setText("欢迎"+ intent.getStringExtra("msg")+"同学！");
    }

}
