package com.mingrisoft.interactactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {


    private TextView msg;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        msg = (TextView) findViewById(R.id.receiveMsg);
        btn = (Button) findViewById(R.id.feedbackBtn);

        Intent intent = getIntent();

        msg.setText(intent.getIntExtra("num",0)  + ": "
                + intent.getStringExtra("msg"));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, MainActivity.class);
                intent.putExtra("msg", "I'm fine, thank you!");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
