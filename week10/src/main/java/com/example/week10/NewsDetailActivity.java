package com.example.week10;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class NewsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        //取出 Intent 中附加的 Bundle
        Bundle bundle = getIntent().getExtras(); if (bundle != null) {
            //创建 Fragment 对象
            NewsDetailFragment detailFragment = new NewsDetailFragment();
            //设置传递到 framgment 中的参数
            detailFragment.setArguments(bundle);
            // 获取所在 fragment 的父容器的管理器
            FragmentManager manager = getSupportFragmentManager();
            //获得 Fragment 事务对象
            FragmentTransaction transaction = manager.beginTransaction();
            //把 detail_fragment_container 替换成新 detailFragment
            transaction.replace(R.id.detail_fragment_container,  detailFragment);
            //提交事务
            transaction.commit();
            //在事务提交后，会进到 framment 的创建生命周期
        }
    }
}