package com.example.week10;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;

public class NewsActivity extends AppCompatActivity implements NewsListFragment.OnNewsSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
    }

    @Override
    public void onPointerCaptureChanged(
            boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    @Override
    public void onNewsSelected(Bundle bundle) {
        // 获取所在 fragment 的父容器的管理器
        FragmentManager manager = getSupportFragmentManager();
        // 获得要操作的 fragment 中的组件
        // 若在平板中运行，匹配的 layout 是 large/activity_news.xml, R.id.fragment_detail 存在，返回非 null
        //  若在手机中运行，匹配的 layout 是 activity_news.xml, R.id.fragment_detail 存在，返回非 null
        Fragment detailFragment = manager.findFragmentById(R.id.fragment_detail);

        //根据 detailFragment 是否为 null，判断是在平板上还是手机上运行
        if(detailFragment != null ){
            //在平板上运行时执行 NewsDetailFragment 类的 setNews()方法显示
            NewsDetailFragment newsDetailFragment = (NewsDetailFragment) detailFragment;
            newsDetailFragment.setNews(bundle.getInt("position",0));
        }else{	//在手机上运行时，打开新的 Activity
            Intent intent = new Intent(this, NewsDetailActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

}