package com.example.week10;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

    private Context context;
    //一组 Image 的 Id
    private int[] imageIds;
    public ImageAdapter(Context context,int[] mThumbIds) { this.context = context;
        this.imageIds = mThumbIds;
    }

    @Override
    public int getCount() { return imageIds.length;
    }

    @Override
    public Object getItem(int position) { return imageIds[position];
    }

    @Override
    public long getItemId(int position) { return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 定义一个 ImageView,显示在 GridView 里
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(300, 300));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(imageIds[position]); return imageView;
    }
}
