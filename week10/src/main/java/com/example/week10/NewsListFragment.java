package com.example.week10;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class NewsListFragment extends Fragment {

    //自定义一个接口，将用于给 activity 传递一个 listview 点击事件
    //更详细的原理，教材 P148"3.与 Activity  通信"处总结得很好，可在实践前与实践后多次看
    public	interface
            OnNewsSelectedListener{ void
        onNewsSelected(Bundle bundle);
    }

    //定义一个自定义接口的对象
    private OnNewsSelectedListener mOnNewsSelectedListener;

    public NewsListFragment() {
        // Required empty public constructor
    }
    //当 Fragment 对象关联到一个上下文（常为 Activity）时，这个方法将被调用
    //调用 onAttach()后才调用 onCreate()
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // 若 context 是 OnNewsSelectedListener 的实例，即实现了 OnNewsSelectedListener 接口，
        // 则使内部对象 onNewsSelectedListener 获得该 context（在后面 onCreate 方法执行时才不为空）
        // 所以，要求要整合这个 Fragment 的 Activity 必须 implement（实现）OnNewsSelectedListener接口
        if (context instanceof
                OnNewsSelectedListener){ mOnNewsSelectedListener =
                (OnNewsSelectedListener) context;
        }else {
            throw new IllegalArgumentException("Activity must OnNewsSelectedListener");
        }
    }
    //当 Fragment 对象从上下文（常为 Activity）中删除时被调用 @Override
    public void onDetach() {
        mOnNewsSelectedListener = null;
        super.onDetach();
    }

    //当 Fragment 对象创建时调用（已经成功执行 onAttach）
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // 为这个 fragment 找到对应的布局，inflate--充气、膨胀
        // inflate 的作用是将一个 layout.xml 布局文件找出来，将将其变为一个 View 对象
        //  对下面 inflate()方法的进一步解释，见本方法定义之后的块注释
        View ret = inflater.inflate(R.layout.fragment_news_list, container, false);

        // 定义 ListView 列出新闻标题，这个 listView 实例要从 xml 布局文件中的指定控件
        ListView listView = (ListView) ret.findViewById(R.id.news_list);
        //设置点击列表项的事件监听器
        listView.setOnItemClickListener(listener);

        // 设置新闻列表内容的匹配器
        if (listView != null){
            //   为简化系统，重点落在解决方案的机制上，这里“生成”新闻，是十足的假新闻
            //   是否动心构思一个用文件系统或数据库提供新闻服务的方案？定个目标，做一做吧
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                data.add("新闻标题"+((i<10)?"0"+i:i));
            }
            // 定义适配器，并指定到新闻列表中
            ArrayAdapter<String> mAdapter = new
                    ArrayAdapter<String>( getContext(),android.R.layout.
                    simple_list_item_1, data
            );
            listView.setAdapter(mAdapter);
        }
        return ret;
    }

    //设置处理列表项点击的事件监听器
    private AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //  当 Fragment 对象创建时调用（这一点能保证，因为已经成功执行了 onAttach）
            if (mOnNewsSelectedListener != null)
            { Bundle bundle = new Bundle();
                bundle.putInt("position",position);
                bundle.putLong("id",id);
                // 向自定义事件监听器传送新闻
                mOnNewsSelectedListener.onNewsSelected(bundle);
            }
        }
    };
}