package com.example.week12_networkprogramming;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;




public class URLConnectionActivity extends AppCompatActivity {

    String urlstr = "http://192.168.0.183:8080/mypages/";

    TextView showSource;

    WebView showWeb, goYtu;

    String response;

    Handler handler=new Handler() { @Override
    public void handleMessage(Message msg) { if(msg.what ==0x123){showSource.setText(response);
    }
        super.handleMessage(msg);
    }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); setContentView(R.layout.activity_urlconnection);

        showSource= findViewById(R.id.showSource); showWeb= findViewById(R.id.showWeb); goYtu = findViewById(R.id.goLink);

        findViewById(R.id.get).setOnClickListener(new View.OnClickListener() { @Override
        public void onClick(View v) {
            new Thread(){
                @Override
                public void run() {
                    response=GetPostUtil.sendGet(urlstr + "getpage.jsp","name=YTUer");
                    handler.sendEmptyMessage(0x123);
                }
            };

        }
        });
        showWeb.loadUrl(urlstr + "getpage.jsp?name=YTUer");
        findViewById(R.id.post).setOnClickListener(new View.OnClickListener() { @Override
        public void onClick(View v) {
            new Thread(){@Override
            public void run() {
                response=GetPostUtil.sendPost( urlstr + "postpage.jsp","name=ytu&pwd=123456");
                handler.sendEmptyMessage(0x123);
            }
            }.start();
            showWeb.loadUrl(urlstr + "postpage.jsp?name=ytu&pwd=123456");
        }
        });
        StringBuffer htmlBuffer = new StringBuffer(); htmlBuffer.append("<html>");
        htmlBuffer.append("<body>点击<a href=\"http://www.ytu.edu.cn\">烟大</a></body>"); htmlBuffer.append("</html>");
        goYtu.loadDataWithBaseURL("",htmlBuffer.toString(),"text/html","UTF-8","");
    }
}