package com.example.week12_networkprogramming;

import androidx.appcompat.app.AppCompatActivity;

import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

public class MysqlConnectionActivity extends AppCompatActivity {
    private EditText readerNumEdt, readerNameEdt, readerPhoneEdt;
    private TextView resultTxt;
    private RadioGroup readerTypeRadioGroup;
    Reader reader = null;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x11:
                    String s = (String) msg.obj;
                    resultTxt.setText(s);
                    break;
                case 0x12:
                    String ss = (String) msg.obj;
                    resultTxt.setText(ss);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mysql_connection);
        readerNumEdt = findViewById(R.id.readerNumEdt);
        readerNameEdt = findViewById(R.id.readerNameEdt);
        readerPhoneEdt = findViewById(R.id.readerPhoneEdt);
        resultTxt = findViewById(R.id.resulteTxt);
        readerTypeRadioGroup = findViewById(R.id.readerTypeRadioGroup);
        findViewById(R.id.queryReaderBnt).setOnClickListener(OnClickListener);
    }

    private View.OnClickListener OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Message message = handler.obtainMessage();
            message.what = 0x11;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd " + "HH:mm:ss");
            switch (view.getId()) {
                case R.id.addReaderBnt:
                    break;
                case R.id.deleteReaderBnt:
                    break;
                case R.id.updateReaderBnt:
                    break;
                case R.id.queryReaderBnt:
                    new Thread(new Runnable() {
                        public void run() {
                            ResultSet res = DBUtils.queryReader(readerNumEdt.getText().toString());
                            String str = "";
                            int i = 1;
                            try {
                                while (res.next()) {
                                    Reader reader = new Reader();
                                    reader.setReader_number(res.getString("reader_number"));
                                    reader.setReader_name(res.getString("reader_name"));
                                    reader.setReader_type(res.getString("reader_type"));
                                    reader.setReader_phone(res.getString("reader_phone"));
                                    reader.setReader_password(res.getString("reader_password"));
                                    reader.setReader_createtime(String.valueOf(((ResultSet) res).getDate("reader_createtime")));
                                    str = str + i + "." + reader.toString();
                                    i++;
                                }
                            } catch (SQLException | java.sql.SQLException throwables) {
                                throwables.printStackTrace();
                            }
                            if (str != "") {
                                message.what = 0x12;
                                message.obj = "查询结果:\n" + str;
                            } else {
                                message.obj = "查询结果为空";
                            }
                            handler.sendMessage(message);
                        }
                    }).start();
                    break;
            }
        }
    };
}