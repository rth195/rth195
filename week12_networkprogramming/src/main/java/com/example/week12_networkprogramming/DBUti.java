package com.example.week12_networkprogramming;

import android.database.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import java.sql.DriverManager;
import java.sql.ResultSet;

class DBUtils { private static String user = "root";
   private static String password = "mypassword";
   private static Connection getConn(String dbName) { Connection connection = null;
      try { Class.forName("com.mysql.jdbc.Driver");
         String url = "jdbc:mysql://192.168.0.183:3306/" + dbName;
         connection = (Connection) DriverManager.getConnection(url, user, password); }
      catch (Exception e) { e.printStackTrace(); }return connection; }
   public static ResultSet queryReader(String reader_num) {  ResultSet rs = null;
      Connection connection = getConn("library"); if (connection != null) {try { PreparedStatement ps;
      Statement smt = (Statement) connection.createStatement();
      String sql = "select * from readers where reader_number LIKE '%" + reader_num +"%'"; rs = smt.executeQuery(sql);
      connection.close(); } catch (SQLException | java.sql.SQLException throwables) { throwables.printStackTrace(); } }return rs;} }