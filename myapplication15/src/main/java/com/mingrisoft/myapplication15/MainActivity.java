package com.mingrisoft.myapplication15;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //校徽对应的 ImageView
    private ImageView flagImageView;
    private TextView flagTxt;
    //上一页和下一页
    private ImageButton backImageBtn;
    private	ImageButton forwardImageBtn;
    //校徽数组（图片及文字）
    //我的大学相关的按钮及显示结果的文本框
    private Button chooseMyUniBtn;
    private TextView chooseMyUniTxt;
    //用于保存选中的我的大学
    //喜欢的大学相关的按钮及显示结果的文本框
    private Button likeUniBtn;
    private TextView likeUniTxt;
    //用于保存选中的我喜欢的大学
    private boolean[]likeUniversitys={false,false,false};
    private String myUniversity;
    private int[] flag = {R.drawable.ytu_256, R.drawable.ytu_128, R.drawable.ytu_bg};
    private String[] universityNames = {"烟台大学", "北京大学", "清华大学"};
    //当前页 默认第一页
    private int currentPage = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //初始化图片、校徽名称组件
        flagImageView = (ImageView) findViewById(R.id.universityImageView);
        flagTxt = (TextView) findViewById(R.id.universityTxt);
//初始化上一页、下一页按钮组件并注册监听器
        backImageBtn = (ImageButton) findViewById(R.id.backImageBtn);
        forwardImageBtn = (ImageButton) findViewById(R.id.forwardImageBtn);
        backImageBtn.setOnClickListener(onClickListener);
        forwardImageBtn.setOnClickListener(onClickListener);
        //初始化显示"我的大学"的按钮并注册监听器
        chooseMyUniBtn=(Button) findViewById(R.id.chooseUniBtn);
        chooseMyUniTxt=(TextView) findViewById(R.id.chooseUniTxt);
        chooseMyUniBtn.setOnClickListener(onClickListener);
        //初始化显示"我喜欢的大学"的按钮并注册监听器
                likeUniBtn=(Button) findViewById(R.id.likeUniBtn);
                likeUniTxt=(TextView) findViewById(R.id.likeUniTxt);
                likeUniBtn.setOnClickListener(onClickListener);
    }
    //定义处理上一页、下一页按钮等点击事件的逻辑
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.backImageBtn://处理“上一页”
                if(currentPage==0){
                    Toast.makeText(MainActivity.this,"第一页，前面没有了",Toast.LENGTH_SHORT).show();
                    //改用普通对话框提示，定义对话框对象
                    AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("第一页，前面没有了");
                    builder.setTitle("提示");
                    //定义确认按钮的属性及单击后事件的处理逻辑
                    builder.setPositiveButton("确认",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog,int i){dialog.dismiss();
                        }
                    });//在第一轮改造程序并运行体验对话框后，在此处加入附后的代码继续体验
                    builder.create().show();

                    return;
                }
                //上翻一页
                currentPage--;//设置校徽图片，在代码中设置和改变图片的方法//
                flagImageView.setImageResource(flag[currentPage]);
                //设置学校名字
                flagTxt.setText(universityNames[currentPage]);break;
                case R.id.forwardImageBtn://处理“下一页”
                if(currentPage==(flag.length-1)){Toast.makeText(MainActivity.this,"最后一页，后面没有了",Toast.LENGTH_SHORT).show();
                    return;
                }
//下翻一页
                    case R.id.chooseUniBtn://处理“我的大学”
                        // 在下面体验内容型对话框
                        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);myUniversity="";
                        builder.setTitle("选择我的大学").setIcon(R.drawable.ytu_16).setSingleChoiceItems(universityNames,//选项来自于universityNames数组
                                 -1,//默认为0表示选中第一个项目,-1代表无预选项目
                                 new DialogInterface.OnClickListener(){
                                     @Override
                                     public void onClick(DialogInterface dialog,int which){
                                         myUniversity=universityNames[which].toString();
                                     }
                                 }).setPositiveButton("确认",new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog,int which){if(myUniversity==""){chooseMyUniTxt.setText("选了个寂寞");}
                            else{chooseMyUniTxt.setText(myUniversity);
                                dialog.dismiss();}}}).setNegativeButton("取消",new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog,int which){chooseMyUniTxt.setText("放弃了选择");dialog.dismiss();}}).create().show();break;
                case R.id.likeUniBtn://处理“我喜欢的大学”
                    // 在下面体验内容型对话框（复选框）
                    AlertDialog.Builder builder2=new AlertDialog.Builder(MainActivity.this);
                    builder2.setTitle("选择我喜欢的大学").setIcon(R.drawable.ytu_16).setMultiChoiceItems(universityNames,likeUniversitys,new DialogInterface.OnMultiChoiceClickListener(){
                        @Override
                        public void onClick(DialogInterface dialogInterface,int i,boolean b){likeUniversitys[i]=b;}}).setPositiveButton("确认",new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog,int which){String univesitysChoosed="";for(int i=0;i<likeUniversitys.length;i++){if(likeUniversitys[i]==true){univesitysChoosed+=(universityNames[i]+"");}}if(univesitysChoosed=="")univesitysChoosed="静待你有所喜欢";likeUniTxt.setText(univesitysChoosed);}}).create().show();break;
                default:
                    break;

            }
        }
    };
}