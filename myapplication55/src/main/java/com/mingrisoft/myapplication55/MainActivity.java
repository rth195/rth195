package com.mingrisoft.myapplication55;

        import android.content.Intent;
        import android.net.Uri;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.ImageButton;
        import android.widget.SimpleAdapter;

public class MainActivity extends AppCompatActivity {

    private ImageButton linksBnt;//收集链接
    private ImageButton openPageBnt;   //网上冲浪
    private ImageButton dialBnt;       //打电话
    private ImageButton mapBnt;        //查地图
    private ImageButton emailBnt;      //发邮件
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //初始化收集链接按钮并注册监听器
        linksBnt = (ImageButton) findViewById(R.id.linksBnt);
        linksBnt.setOnClickListener(buttonOnClickListener);
        //初始化网上冲浪按钮并注册监听器
        openPageBnt = (ImageButton) findViewById(R.id.openPageBnt);
        openPageBnt.setOnClickListener(buttonOnClickListener);

        //初始化查地图按钮并注册监听器
        mapBnt = (ImageButton) findViewById(R.id.mapBnt);
        mapBnt.setOnClickListener(buttonOnClickListener);

        //初始化打电话按钮并注册监听器
        dialBnt = (ImageButton) findViewById(R.id.dialBnt);
        dialBnt.setOnClickListener(buttonOnClickListener);

        //初始化发邮件按钮并注册监听器
        emailBnt = (ImageButton) findViewById(R.id.emailBnt);
        emailBnt.setOnClickListener(buttonOnClickListener);
    }

    //定义事件监听的处理逻辑
    private View.OnClickListener buttonOnClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    Uri data;
                    switch(view.getId()){
                        case R.id.linksBnt: //收集链接
                            intent.setClass(MainActivity.this, LinksActivity.class);
                            //下面一行用于后面的“体验标志位”要求，见后说明
                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            break;
                        case R.id.openPageBnt: //打开网页
                            intent.setAction(Intent.ACTION_VIEW);
                            data = Uri.parse("http://www.ytu.edu.cn");
                            intent.setData(data);
                            // 上面分步的设置，与下面的写法等价
                            // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ytu.edu.cn"));
                            startActivity(intent);
                            break;
                        case R.id.mapBnt: //查地图
                            intent.setAction(Intent.ACTION_VIEW);
                            data = Uri.parse("geo:37.465108105834844, 121.47892736523434");
                            intent.setData(data);
                            startActivity(intent);
                            break;
                        case R.id.dialBnt: //打电话
                            intent.setAction(Intent.ACTION_DIAL).setData(Uri.parse("tel:13912345678"));
                            startActivity(intent);
                            break;
                        case R.id.emailBnt: //发邮件
                            intent.setAction(Intent.ACTION_SENDTO);
                            data = Uri.parse("mailto:sxhelijian@163.com");
                            intent.setData(data);
                            startActivity(intent);
                            break;
                    }
                }
            };
}