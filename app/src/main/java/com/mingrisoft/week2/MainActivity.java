package com.mingrisoft.week2;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity{

    //①为要操作的控件定义相应的对象；
    private Button clickBtn;
    private TextView showText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //②初始化组件，并为每一个需要监听的控件编写事件处理代码。
        showText = (TextView) findViewById(R.id.showText);
        clickBtn = (Button) findViewById(R.id.clickBtn);

        clickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showText.setText("按钮被单击！");
            }
        });

        showText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "文本框被点了...",Toast.LENGTH_LONG).show();
            }
        });
    }
}
